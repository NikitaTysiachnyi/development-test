import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  options = {
    bottom: 0,
    fixed: true,
    top: 0
  };

  elements = [
    {size: '150mm'},
    {size: '250mm'},
    {size: '350mm'},
    {size: '450mm'},
    {size: '550mm'},
    {size: '650mm'}
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
